package cn.osworks.aos.modules.bpm.dao.po;

import cn.osworks.aos.base.typewrap.PO;

/**
 * <b>[aos_act_hi_identitylink]数据持久化对象</b>
 * <p>
 * 注意:此类代码自动生成-禁止手工修改。
 * </p>
 * 
 * @author OSWorks-XC
 * @date 2015-01-07 23:01:59
 */
public class Aos_act_hi_identitylinkPO extends PO {

	private static final long serialVersionUID = 1L;

	/**
	 * id_
	 */
	private String id_;
	
	/**
	 * group_id_
	 */
	private String group_id_;
	
	/**
	 * type_
	 */
	private String type_;
	
	/**
	 * user_id_
	 */
	private String user_id_;
	
	/**
	 * task_id_
	 */
	private String task_id_;
	
	/**
	 * proc_inst_id_
	 */
	private String proc_inst_id_;
	

	/**
	 * id_
	 * 
	 * @return id_
	 */
	public String getId_() {
		return id_;
	}
	
	/**
	 * group_id_
	 * 
	 * @return group_id_
	 */
	public String getGroup_id_() {
		return group_id_;
	}
	
	/**
	 * type_
	 * 
	 * @return type_
	 */
	public String getType_() {
		return type_;
	}
	
	/**
	 * user_id_
	 * 
	 * @return user_id_
	 */
	public String getUser_id_() {
		return user_id_;
	}
	
	/**
	 * task_id_
	 * 
	 * @return task_id_
	 */
	public String getTask_id_() {
		return task_id_;
	}
	
	/**
	 * proc_inst_id_
	 * 
	 * @return proc_inst_id_
	 */
	public String getProc_inst_id_() {
		return proc_inst_id_;
	}
	

	/**
	 * id_
	 * 
	 * @param id_
	 */
	public void setId_(String id_) {
		this.id_ = id_;
	}
	
	/**
	 * group_id_
	 * 
	 * @param group_id_
	 */
	public void setGroup_id_(String group_id_) {
		this.group_id_ = group_id_;
	}
	
	/**
	 * type_
	 * 
	 * @param type_
	 */
	public void setType_(String type_) {
		this.type_ = type_;
	}
	
	/**
	 * user_id_
	 * 
	 * @param user_id_
	 */
	public void setUser_id_(String user_id_) {
		this.user_id_ = user_id_;
	}
	
	/**
	 * task_id_
	 * 
	 * @param task_id_
	 */
	public void setTask_id_(String task_id_) {
		this.task_id_ = task_id_;
	}
	
	/**
	 * proc_inst_id_
	 * 
	 * @param proc_inst_id_
	 */
	public void setProc_inst_id_(String proc_inst_id_) {
		this.proc_inst_id_ = proc_inst_id_;
	}
	

}