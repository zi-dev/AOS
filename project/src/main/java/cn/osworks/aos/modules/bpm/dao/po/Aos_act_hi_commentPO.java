package cn.osworks.aos.modules.bpm.dao.po;

import java.util.Date;

import cn.osworks.aos.base.typewrap.PO;

/**
 * <b>[aos_act_hi_comment]数据持久化对象</b>
 * <p>
 * 注意:此类代码自动生成-禁止手工修改。
 * </p>
 * 
 * @author OSWorks-XC
 * @date 2015-01-07 23:01:58
 */
public class Aos_act_hi_commentPO extends PO {

	private static final long serialVersionUID = 1L;

	/**
	 * id_
	 */
	private String id_;
	
	/**
	 * type_
	 */
	private String type_;
	
	/**
	 * time_
	 */
	private Date time_;
	
	/**
	 * user_id_
	 */
	private String user_id_;
	
	/**
	 * task_id_
	 */
	private String task_id_;
	
	/**
	 * proc_inst_id_
	 */
	private String proc_inst_id_;
	
	/**
	 * action_
	 */
	private String action_;
	
	/**
	 * message_
	 */
	private String message_;
	
	/**
	 * full_msg_
	 */
	private byte[] full_msg_;
	

	/**
	 * id_
	 * 
	 * @return id_
	 */
	public String getId_() {
		return id_;
	}
	
	/**
	 * type_
	 * 
	 * @return type_
	 */
	public String getType_() {
		return type_;
	}
	
	/**
	 * time_
	 * 
	 * @return time_
	 */
	public Date getTime_() {
		return time_;
	}
	
	/**
	 * user_id_
	 * 
	 * @return user_id_
	 */
	public String getUser_id_() {
		return user_id_;
	}
	
	/**
	 * task_id_
	 * 
	 * @return task_id_
	 */
	public String getTask_id_() {
		return task_id_;
	}
	
	/**
	 * proc_inst_id_
	 * 
	 * @return proc_inst_id_
	 */
	public String getProc_inst_id_() {
		return proc_inst_id_;
	}
	
	/**
	 * action_
	 * 
	 * @return action_
	 */
	public String getAction_() {
		return action_;
	}
	
	/**
	 * message_
	 * 
	 * @return message_
	 */
	public String getMessage_() {
		return message_;
	}
	
	/**
	 * full_msg_
	 * 
	 * @return full_msg_
	 */
	public byte[] getFull_msg_() {
		return full_msg_;
	}
	

	/**
	 * id_
	 * 
	 * @param id_
	 */
	public void setId_(String id_) {
		this.id_ = id_;
	}
	
	/**
	 * type_
	 * 
	 * @param type_
	 */
	public void setType_(String type_) {
		this.type_ = type_;
	}
	
	/**
	 * time_
	 * 
	 * @param time_
	 */
	public void setTime_(Date time_) {
		this.time_ = time_;
	}
	
	/**
	 * user_id_
	 * 
	 * @param user_id_
	 */
	public void setUser_id_(String user_id_) {
		this.user_id_ = user_id_;
	}
	
	/**
	 * task_id_
	 * 
	 * @param task_id_
	 */
	public void setTask_id_(String task_id_) {
		this.task_id_ = task_id_;
	}
	
	/**
	 * proc_inst_id_
	 * 
	 * @param proc_inst_id_
	 */
	public void setProc_inst_id_(String proc_inst_id_) {
		this.proc_inst_id_ = proc_inst_id_;
	}
	
	/**
	 * action_
	 * 
	 * @param action_
	 */
	public void setAction_(String action_) {
		this.action_ = action_;
	}
	
	/**
	 * message_
	 * 
	 * @param message_
	 */
	public void setMessage_(String message_) {
		this.message_ = message_;
	}
	
	/**
	 * full_msg_
	 * 
	 * @param full_msg_
	 */
	public void setFull_msg_(byte[] full_msg_) {
		this.full_msg_ = full_msg_;
	}
	

}