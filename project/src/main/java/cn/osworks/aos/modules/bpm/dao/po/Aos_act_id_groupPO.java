package cn.osworks.aos.modules.bpm.dao.po;

import cn.osworks.aos.base.typewrap.PO;

/**
 * <b>[aos_act_id_group]数据持久化对象</b>
 * <p>
 * 注意:此类代码自动生成-禁止手工修改。
 * </p>
 * 
 * @author OSWorks-XC
 * @date 2015-01-07 23:01:59
 */
public class Aos_act_id_groupPO extends PO {

	private static final long serialVersionUID = 1L;

	/**
	 * id_
	 */
	private String id_;
	
	/**
	 * rev_
	 */
	private Integer rev_;
	
	/**
	 * name_
	 */
	private String name_;
	
	/**
	 * type_
	 */
	private String type_;
	

	/**
	 * id_
	 * 
	 * @return id_
	 */
	public String getId_() {
		return id_;
	}
	
	/**
	 * rev_
	 * 
	 * @return rev_
	 */
	public Integer getRev_() {
		return rev_;
	}
	
	/**
	 * name_
	 * 
	 * @return name_
	 */
	public String getName_() {
		return name_;
	}
	
	/**
	 * type_
	 * 
	 * @return type_
	 */
	public String getType_() {
		return type_;
	}
	

	/**
	 * id_
	 * 
	 * @param id_
	 */
	public void setId_(String id_) {
		this.id_ = id_;
	}
	
	/**
	 * rev_
	 * 
	 * @param rev_
	 */
	public void setRev_(Integer rev_) {
		this.rev_ = rev_;
	}
	
	/**
	 * name_
	 * 
	 * @param name_
	 */
	public void setName_(String name_) {
		this.name_ = name_;
	}
	
	/**
	 * type_
	 * 
	 * @param type_
	 */
	public void setType_(String type_) {
		this.type_ = type_;
	}
	

}