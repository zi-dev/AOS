### 提示
请在【[版本->标签](http://git.oschina.net/osworks/AOS/tags)】页面下载正式release出的发行版作为项目的开发基线。缺省的master主干是AOS的开发版分支，你可以通过git或svn与我们保持同步更新，但不提供此分支的可用性承诺与支持。
### AOS平台简介
AOS应用基础平台基于JavaEE技术体系，以“标准功能可复用、通用模块可配置、行业需求快速开发、异构系统无缝集成”为目标，为软件开发团队提供高效可控、随需应变、快速实现业务需求的全栈式技术解决方案。[点击加入QQ交流群(92497522)](http://jq.qq.com/?_wv=1027&k=bv0bWU)
##### AOS在线演示系统 
[http://42.120.21.17/aos](http://42.120.21.17/aos)  (在线演示系统为只读模式，部分功能可能不流畅。完美体验请[搭建本地环境](http://git.oschina.net/osworks/AOS/raw/master/doc/AOS%E5%AE%89%E8%A3%85%E8%AF%B4%E6%98%8E.pdf)。)<br>
登录帐户/密码：root/111111。(或单击 开发者 按钮直接登录)
##### AOS开发者社区 
即将上线……
### 产品定位与用户价值
AOS应用基础平台面向各型软件开发团队，通过提供卓越的基础平台和服务支持保障。帮助企业落实IT策略、屏蔽技术壁垒快速实现业务愿景，使其获得更低成本、更高质量、更快交付业务和运维支持的核心技术竞争力。
### 适用行业及项目场景
适合构建和开发中大型企业管理软件和各级政务信息化系统。如各类型企业的ERP/CRM/OA/HR/MRP/WMS等软件；各级政府/事业单位(机关、公安、财政、税务、保险、医疗等)的审批办公、业务经办系统；移动App后端支撑系统；微信公众号开发等。
### 特性与优势
基于成熟的JavaEE技术体系标准；面向服务的可伸缩弹性架构；完善的组织机构权限管理、主数据管理；业务需求构件化开发、业务流程可视化装配；标准模块缺省搭载、通用模块高度可配；代码可视化生成、异构系统无缝集成、部署方案随需应变。
### 功能组成
AOS核心类库(模版引擎 规则引擎 数据访问组件)、资源数据缓存组件、系统安全服务、复杂事件处理、工作流/可视化设计器、系统审计监控组件、服务治理组件、AOS标签库、组织机构与权限管理、标准功能实现、通用模块参考实现、开发控制台……
### 环境依赖
Java环境：Java6+ Servlet2.5+。<br>
数据库：Oracle DB2 PostgreSQL SQL-Server MySQL MariaDB H2。<br>
应用服务器：Weblogic WebSphere JBoss Tomcat Jetty等<br>
服务器OS：IBM-AIX HP-Unix Linux系列 Windows。<br>
客户端：IE8+ Chrome FireFox App 桌面应用等。
### 作者微博互粉
[http://weibo.com/afghan007](http://weibo.com/afghan007)
### AOS部分截屏 
![github](http://dl2.iteye.com/upload/attachment/0109/7699/6f8c148a-6ada-3889-a740-d9439ccd299e.gif "AOS截屏")